s = int(raw_input())

def f_equalx(l_ammounts, v_count_frend, v_max_ammount):
    v_tmp = 0
    for i in range(v_count_frend):
        v_tmp = l_ammounts[i]
        v_amount_allow = 0
        if v_tmp == v_max_ammount:
            return "YES"
            exit()
        for n in range((i + 1), v_count_frend):
            v_amount_allow += (v_tmp + l_ammounts[n])
            v_tmp = 0
            if v_amount_allow == v_max_ammount:
                return "YES"
                exit()

    return "NO"

for n in range(s):
    v_input1 = raw_input()
    l_frends_amounts = ""
    v_sum_amount = 0
    l_frends_amounts = v_input1.split(" ")
    l_ammount_frend = []

    for i in range(int(l_frends_amounts[0])):
        v_input2 = int(raw_input())
        l_ammount_frend.append(v_input2)

    print f_equalx(l_ammount_frend, int(l_frends_amounts[0]), int(l_frends_amounts[1]))
